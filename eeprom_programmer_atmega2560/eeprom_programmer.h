union u32u {
        uint32_t l;
        uint8_t b[4];
};
union u16u {
        uint16_t l;
        uint8_t b[2];
};

static uint8_t flash_databus_read(void);
static void flash_databus_tristate(void);
static void flash_databus_output(u16u data);
void flash_init(void);
static void flash_setaddr(uint32_t addr, uint8_t mot);
void flash_write(uint32_t addr, uint8_t data);
int flash_writen(uint32_t addr, uint16_t *data, uint8_t len);
uint8_t flash_read(uint32_t addr);
void flash_readn(uint32_t addr, uint32_t len);
