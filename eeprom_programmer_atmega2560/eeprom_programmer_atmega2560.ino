#include <limits.h>
#include <stdint.h>
#include "eeprom_programmer.h"

/* MX29F1615 programmer using an arduino mega (2560)

    Requirements :
    some transistor/resistor to apply 10V or 5V on pin _BYTE/VPP

    Data:
    Pin 22 to 29, Port A -> Q0 to Q7
    Pin 37 downto 30, Port C -> Q8 to Q15
    Address:
    Pin A0 to A15, ports F, K; pins 10 to 13, high half part of port B -> Address 0 to 19
    Control:
    PIN 4 -> BYPE/VPP control (through a transistor)
    PIN 2 -> !OE
    PIN 3 -> !CE
*/

#define BYTE_VPP 4   // To the gate/base of the transistor for VPP (10V) of 5V (16bits read mode)
#define _OE 2        // Output enable of the flash
#define _CE 3        // Chip Enable of the flash

// Macros to simplify the code
#define FLASH_VPP_LOW digitalWrite(BYTE_VPP, HIGH) // WORD mode (normal)
#define FLASH_VPP_VHH digitalWrite(BYTE_VPP, LOW) // VPP=VHH (10Volts=programming mode)
#define FLASH_OE_HIGH digitalWrite(_OE, HIGH)  // !OE HIGH (disabled)
#define FLASH_OE_LOW digitalWrite(_OE, LOW)  // !OE LOW (enabled)
#define FLASH_CE_HIGH digitalWrite(_CE, HIGH)  // !CE HIGH (disabled)
#define FLASH_CE_LOW digitalWrite(_CE, LOW)  // pin A3 (!CE) LOW (enabled)
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

#define ADDR_MAX 0x1FFFFF // 2^(number of address lines + 1 (8 bit read))-1, 2^21-1
#define ACK 0x06  // used by flashrom...
#define NAK 0x15

/* Setup */
void setup() {
  // initialize serial communication
  //Serial.begin(115200);
  Serial.begin(1000000);
  pinMode(_OE, OUTPUT); digitalWrite(_OE, HIGH);
  pinMode(_CE, OUTPUT); digitalWrite(_CE, HIGH);
  pinMode(BYTE_VPP, OUTPUT); digitalWrite(BYTE_VPP, HIGH); // HIGH = 5V (Read Word mode), LOW = 10V (write mode)
  flash_databus_tristate();
  DDRF = DDRK = DDRB = 255; // Address bus as output
}

void loop() {
  static uint16_t i = 0;
  static uint8_t buf[12];                // Serial buf
  static u32u len = { .l = 0 };          // Easy convertion from bytes to 32 bit word
  static u32u addr_s = { .l = 0 };       // Do not forget to initialize
  static uint8_t write_buf[256];         // Data to write to flash, read from serial port

  if ( ! Serial.available()) {
    _delay_us(100);
  }
  else
  {
    char inChar = (char)Serial.read();
    switch (inChar) {
      case 0x00: Serial.write(ACK); break; //NOP
      case 0x03: Serial.write(ACK); Serial.println("__MegaProg___ "); break;
      case 0x0C: if (Serial.readBytes(buf, 4)) {      // 0x0C, write command at : Address, data, ACK
          addr_s.b[0] = buf[0]; addr_s.b[1] = buf[1]; addr_s.b[2] = buf[2];
          flash_write(addr_s.l, buf[3]);
          Serial.write(ACK);
        }
        else Serial.write(NAK);
        break;             // Write 1 byte

      case 0x42:  // ** Read n bytes **
        if (Serial.readBytes(buf, 6) == 6) {
          addr_s.b[0] = buf[0]; addr_s.b[1] = buf[1]; addr_s.b[2] = buf[2];
          //len.b[0] = buf[3] ; len.b[1] = buf[4] ; len.b[2] = buf[5];
          len.b[0] = buf[3];
          if ((len.l + (addr_s.l)) <= ADDR_MAX + 1)
          {
            Serial.write(ACK);
            flash_readn(addr_s.l, len.l);
          }
          else {
            Serial.write(43);
          }
        }
        else Serial.write(44);
        break;

      case 0x0D: // ** Write n bytes **
        if (Serial.readBytes(buf, 5) == 5) {      // 3 bytes of address start, 2 bytes of length (64 words (16 bits) at a time)
          addr_s.b[0] = buf[0]; addr_s.b[1] = buf[1]; addr_s.b[2] = buf[2];
          len.b[0] = buf[3] ; len.b[1] = buf[4];

          if (len.l <= 256)
          {
            if (len.l == 1) {
              flash_write(addr_s.l, Serial.read());
            }
            else {
              uint8_t incr;
              for ( i = 0; i < len.l; )
              {
                flash_write(0x5555, 0xAA);
                flash_write(0x2AAA, 0x55);
                flash_write(0x5555, 0xA0); // Automatic Page program mode
                (i + 128 < len.l) ? incr = 128 : incr = len.l - i ;
                Serial.readBytes(write_buf, incr);

                if ((flash_writen(addr_s.l, (uint16_t *)write_buf, incr >> 1)) != 0 )     // length of bytes to write, divided by 2 for 16 bit words
                {
                  Serial.write(NAK);
                  break;
                }
                i += incr;
              }
              flash_write(0x5555, 0xAA);
              flash_write(0x2AAA, 0x55);
              flash_write(0x5555, 0xF0); // Return to read mode

              Serial.write(ACK);
            }
          }
        }
        else Serial.write(NAK);
        break;

      default : Serial.write(NAK);
    }
  }
}

/* *********Functions!********* */

static uint8_t flash_databus_read(void) {
  uint8_t data = 0;
  data = PINA;
  return data;
}

static uint16_t flash_databus_read16(void) {
  uint16_t data = 0;
  data = PINC;
  data = data << 8;
  data = PINA | data;
  return data;
}

static void flash_databus_tristate(void) {
  DDRA = DDRC = 0;
}

static void flash_databus_output(u16u data) {
  DDRA = DDRC = 255;
  PORTA = data.b[0];
  PORTC = data.b[1];
}

static void flash_setaddr(uint32_t addr, uint8_t mot) {
  uint32_t c;
  if (mot == 1) { // Switch addr and put the LSB in A-1 (8 bit mode)
    addr = addr >> 1;
  }
  PORTF = addr;
  PORTK = (addr >> 8);
  PORTB = (addr >> (12)); // bits 17 to 19 have to go to pins PB4 to PB7, we just ignore the low 4 bits as they're not plugged to anything.
}

void flash_write(uint32_t addr, uint8_t data) { // From frser API
  // Figure 5, page 20
  u16u dat = { .l = 0 };
  dat.b[0] = data;
  flash_setaddr(addr, 0);
  FLASH_OE_HIGH;
  FLASH_VPP_VHH;
  FLASH_CE_LOW;
  flash_databus_output(dat);
  _delay_us(2);
  FLASH_CE_HIGH;
  FLASH_VPP_LOW;
  flash_databus_tristate();
}

int flash_writen(uint32_t addr, uint16_t *data, uint8_t len) {
  // Figure 5, page 21
  int ret = 0;
  u16u dat = { .l = 0 };
  addr = addr >> 1 ; // 16 bits addr
  FLASH_OE_HIGH;
  FLASH_CE_HIGH;
  FLASH_VPP_VHH;
  for ( uint32_t i = 0; i < len; i++ ) {
    flash_setaddr(addr + i, 0);
    _delay_us(2);
    FLASH_CE_HIGH; // get the data
    FLASH_CE_LOW; // get the address
    dat.l = data[i];
    flash_databus_output(dat);
    _delay_us(1);  // à réduire à 2

  }
  _delay_us(100); // Page 12
  FLASH_VPP_LOW;

  flash_databus_tristate();

  FLASH_CE_LOW;
  do {
    FLASH_OE_HIGH;
    FLASH_OE_LOW;
  }
  while ( digitalRead(29) == LOW ); // Read Q7
  ret = digitalRead(26);
  FLASH_OE_HIGH;
  FLASH_CE_HIGH;
  if ( ret == HIGH ) // Read Q4 (program fail bit)
  {
    return -1;
  }
  return 0;
}

uint8_t flash_read(uint32_t addr) {
  // Figure 3, page 17
  uint8_t data;
  flash_databus_tristate(); // if not already tristate. We need that to read the bus.
  flash_setaddr(addr, 0); // Registers read only, data read with flash_readn
  FLASH_CE_LOW;
  FLASH_OE_LOW;
  //_delay_us(2);
  data = flash_databus_read();
  FLASH_OE_HIGH;
  FLASH_CE_HIGH;
  return data;
}

void flash_readn(uint32_t addr, uint32_t len) {
  u16u mot;

  flash_databus_tristate(); // if not already tristate. We need that to read the bus.
  FLASH_VPP_LOW; // read mode, just in case...
  FLASH_CE_LOW;
  if (len == 1) // 8 bit mode?
  {
    flash_setaddr(addr, 0);
    FLASH_OE_LOW;
    _delay_us(1);
    mot.b[0] = flash_databus_read();
    Serial.write(mot.b[0]);
    FLASH_OE_HIGH;
  }
  else
  {
    addr = addr >> 1; // 16 bit address here
    for (uint32_t i = addr; i < addr + len / 2 ; i++) {

      flash_setaddr(i, 0);
      FLASH_OE_LOW;
      _delay_us(1);
      mot.l = flash_databus_read16();
      Serial.write(mot.b[0]); // MSB first?
      if ( len != 1 ) Serial.write(mot.b[1]);
      FLASH_OE_HIGH;
    }
  }
  FLASH_CE_HIGH;
}
