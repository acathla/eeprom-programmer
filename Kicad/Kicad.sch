EESchema Schematic File Version 4
LIBS:Kicad-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "mar. 31 mars 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 9350 1350
Text Label 9250 1200 1    60   ~ 0
IOREF
Text Label 8900 1200 1    60   ~ 0
Vin
Text Label 8900 2450 0    60   ~ 0
A0
Text Label 8900 2550 0    60   ~ 0
A1
Text Label 8900 2650 0    60   ~ 0
A2
Text Label 8900 2750 0    60   ~ 0
A3
Text Label 8900 2850 0    60   ~ 0
A4
Text Label 8900 2950 0    60   ~ 0
A5
Text Label 8900 3050 0    60   ~ 0
A6
Text Label 8900 3150 0    60   ~ 0
A7
Text Label 8900 3400 0    60   ~ 0
A8
Text Label 8900 3500 0    60   ~ 0
A9
Text Label 8900 3600 0    60   ~ 0
A10
Text Label 8900 3700 0    60   ~ 0
A11
Text Label 8900 3800 0    60   ~ 0
A12
Text Label 8900 3900 0    60   ~ 0
A13
Text Label 8900 4000 0    60   ~ 0
A14
Text Label 8900 4100 0    60   ~ 0
A15
Text Label 10500 4650 1    60   ~ 0
22
Text Label 10400 4650 1    60   ~ 0
24
Text Label 10300 4650 1    60   ~ 0
26
Text Label 10200 4650 1    60   ~ 0
28
Text Label 10100 4650 1    60   ~ 0
30
Text Label 10000 4650 1    60   ~ 0
32
Text Label 9900 4650 1    60   ~ 0
34
Text Label 9800 4650 1    60   ~ 0
36
Text Label 9700 4650 1    60   ~ 0
38
Text Label 9600 4650 1    60   ~ 0
40
Text Label 9500 4650 1    60   ~ 0
42
Text Label 9400 4650 1    60   ~ 0
44
Text Label 9300 4650 1    60   ~ 0
46
Text Label 9200 4650 1    60   ~ 0
48
Text Label 9100 4650 1    60   ~ 0
50(MISO)
Text Label 9000 4650 1    60   ~ 0
52(SCK)
Text Label 10500 5650 1    60   ~ 0
23
Text Label 10400 5650 1    60   ~ 0
25
Text Label 10300 5650 1    60   ~ 0
27
Text Label 10100 5650 1    60   ~ 0
31
Text Label 10200 5650 1    60   ~ 0
29
Text Label 10000 5650 1    60   ~ 0
33
Text Label 9900 5650 1    60   ~ 0
35
Text Label 9800 5650 1    60   ~ 0
37
Text Label 9700 5650 1    60   ~ 0
39
Text Label 9600 5650 1    60   ~ 0
41
Text Label 9500 5650 1    60   ~ 0
43
Text Label 9400 5650 1    60   ~ 0
45
Text Label 9300 5650 1    60   ~ 0
47
Text Label 9200 5650 1    60   ~ 0
49
Text Label 9100 5750 1    60   ~ 0
51(MOSI)
Text Label 9000 5750 1    60   ~ 0
53(SS)
Text Label 10400 4100 0    60   ~ 0
21(SCL)
Text Label 10400 4000 0    60   ~ 0
20(SDA)
Text Label 10400 3900 0    60   ~ 0
19(Rx1)
Text Label 10400 3800 0    60   ~ 0
18(Tx1)
Text Label 10400 3700 0    60   ~ 0
17(Rx2)
Text Label 10400 3600 0    60   ~ 0
16(Tx2)
Text Label 10400 3500 0    60   ~ 0
15(Rx3)
Text Label 10400 3400 0    60   ~ 0
14(Tx3)
Text Label 10400 1550 0    60   ~ 0
13(**)
Text Label 10400 1650 0    60   ~ 0
12(**)
Text Label 10400 1750 0    60   ~ 0
11(**)
Text Label 10400 1850 0    60   ~ 0
10(**)
Text Label 10400 1950 0    60   ~ 0
9(**)
Text Label 10400 2050 0    60   ~ 0
8(**)
Text Label 10400 2450 0    60   ~ 0
7(**)
Text Label 10400 2550 0    60   ~ 0
6(**)
Text Label 10400 2650 0    60   ~ 0
5(**)
Text Label 10400 2750 0    60   ~ 0
4(**)
Text Label 10400 2850 0    60   ~ 0
3(**)
Text Label 10400 2950 0    60   ~ 0
2(**)
Text Label 10400 3050 0    60   ~ 0
1(Tx0)
Text Label 10400 3150 0    60   ~ 0
0(Rx0)
Text Label 10400 1250 0    60   ~ 0
SDA
Text Label 10400 1150 0    60   ~ 0
SCL
Text Label 10400 1350 0    60   ~ 0
AREF
Text Notes 8375 575  0    60   ~ 0
Shield for Arduino Mega Rev 3
Text Notes 10700 1000 0    60   ~ 0
Holes
$Comp
L Connector_Generic:Conn_01x01 P8
U 1 1 56D70B71
P 10600 650
F 0 "P8" V 10700 650 31  0000 C CNN
F 1 "CONN_01X01" V 10700 650 50  0001 C CNN
F 2 "Socket_Arduino_Mega:Arduino_1pin" H 10600 650 50  0001 C CNN
F 3 "" H 10600 650 50  0000 C CNN
	1    10600 650 
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 P9
U 1 1 56D70C9B
P 10700 650
F 0 "P9" V 10800 650 31  0000 C CNN
F 1 "CONN_01X01" V 10800 650 50  0001 C CNN
F 2 "Socket_Arduino_Mega:Arduino_1pin" H 10700 650 50  0001 C CNN
F 3 "" H 10700 650 50  0000 C CNN
	1    10700 650 
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 P10
U 1 1 56D70CE6
P 10800 650
F 0 "P10" V 10900 650 31  0000 C CNN
F 1 "CONN_01X01" V 10900 650 50  0001 C CNN
F 2 "Socket_Arduino_Mega:Arduino_1pin" H 10800 650 50  0001 C CNN
F 3 "" H 10800 650 50  0000 C CNN
	1    10800 650 
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 P11
U 1 1 56D70D2C
P 10900 650
F 0 "P11" V 11000 650 31  0000 C CNN
F 1 "CONN_01X01" V 11000 650 50  0001 C CNN
F 2 "Socket_Arduino_Mega:Arduino_1pin" H 10900 650 50  0001 C CNN
F 3 "" H 10900 650 50  0000 C CNN
	1    10900 650 
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 P12
U 1 1 56D711A2
P 11000 650
F 0 "P12" V 11100 650 31  0000 C CNN
F 1 "CONN_01X01" V 11100 650 50  0001 C CNN
F 2 "Socket_Arduino_Mega:Arduino_1pin" H 11000 650 50  0001 C CNN
F 3 "" H 11000 650 50  0000 C CNN
	1    11000 650 
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 P13
U 1 1 56D711F0
P 11100 650
F 0 "P13" V 11200 650 31  0000 C CNN
F 1 "CONN_01X01" V 11200 650 50  0001 C CNN
F 2 "Socket_Arduino_Mega:Arduino_1pin" H 11100 650 50  0001 C CNN
F 3 "" H 11100 650 50  0000 C CNN
	1    11100 650 
	0    -1   -1   0   
$EndComp
NoConn ~ 10600 850 
NoConn ~ 10700 850 
NoConn ~ 10800 850 
NoConn ~ 10900 850 
NoConn ~ 11000 850 
NoConn ~ 11100 850 
$Comp
L Connector_Generic:Conn_01x08 P2
U 1 1 56D71773
P 9550 1650
F 0 "P2" H 9550 2050 50  0000 C CNN
F 1 "Power" V 9650 1650 50  0000 C CNN
F 2 "Socket_Arduino_Mega:Socket_Strip_Arduino_1x08" H 9550 1650 50  0001 C CNN
F 3 "" H 9550 1650 50  0000 C CNN
	1    9550 1650
	1    0    0    -1  
$EndComp
Text Notes 9650 1350 0    60   ~ 0
1
$Comp
L power:+3V3 #PWR01
U 1 1 56D71AA9
P 9100 1200
F 0 "#PWR01" H 9100 1050 50  0001 C CNN
F 1 "+3.3V" V 9100 1450 50  0000 C CNN
F 2 "" H 9100 1200 50  0000 C CNN
F 3 "" H 9100 1200 50  0000 C CNN
	1    9100 1200
	1    0    0    -1  
$EndComp
Text Label 8600 1550 0    60   ~ 0
Reset
$Comp
L power:+5V #PWR02
U 1 1 56D71D10
P 9000 1050
F 0 "#PWR02" H 9000 900 50  0001 C CNN
F 1 "+5V" V 9000 1250 50  0000 C CNN
F 2 "" H 9000 1050 50  0000 C CNN
F 3 "" H 9000 1050 50  0000 C CNN
	1    9000 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 56D721E6
P 9250 2150
F 0 "#PWR03" H 9250 1900 50  0001 C CNN
F 1 "GND" H 9250 2000 50  0000 C CNN
F 2 "" H 9250 2150 50  0000 C CNN
F 3 "" H 9250 2150 50  0000 C CNN
	1    9250 2150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x10 P5
U 1 1 56D72368
P 9950 1550
F 0 "P5" H 9950 2050 50  0000 C CNN
F 1 "PWM" V 10050 1550 50  0000 C CNN
F 2 "Socket_Arduino_Mega:Socket_Strip_Arduino_1x10" H 9950 1550 50  0001 C CNN
F 3 "" H 9950 1550 50  0000 C CNN
	1    9950 1550
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 56D72A3D
P 10250 2150
F 0 "#PWR04" H 10250 1900 50  0001 C CNN
F 1 "GND" H 10250 2000 50  0000 C CNN
F 2 "" H 10250 2150 50  0000 C CNN
F 3 "" H 10250 2150 50  0000 C CNN
	1    10250 2150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 P3
U 1 1 56D72F1C
P 9550 2750
F 0 "P3" H 9550 3150 50  0000 C CNN
F 1 "Analog" V 9650 2750 50  0000 C CNN
F 2 "Socket_Arduino_Mega:Socket_Strip_Arduino_1x08" H 9550 2750 50  0001 C CNN
F 3 "" H 9550 2750 50  0000 C CNN
	1    9550 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 P6
U 1 1 56D734D0
P 9950 2750
F 0 "P6" H 9950 3150 50  0000 C CNN
F 1 "PWM" V 10050 2750 50  0000 C CNN
F 2 "Socket_Arduino_Mega:Socket_Strip_Arduino_1x08" H 9950 2750 50  0001 C CNN
F 3 "" H 9950 2750 50  0000 C CNN
	1    9950 2750
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 P4
U 1 1 56D73A0E
P 9550 3700
F 0 "P4" H 9550 4100 50  0000 C CNN
F 1 "Analog" V 9650 3700 50  0000 C CNN
F 2 "Socket_Arduino_Mega:Socket_Strip_Arduino_1x08" H 9550 3700 50  0001 C CNN
F 3 "" H 9550 3700 50  0000 C CNN
	1    9550 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 P7
U 1 1 56D73F2C
P 9950 3700
F 0 "P7" H 9950 4100 50  0000 C CNN
F 1 "Communication" V 10050 3700 50  0000 C CNN
F 2 "Socket_Arduino_Mega:Socket_Strip_Arduino_1x08" H 9950 3700 50  0001 C CNN
F 3 "" H 9950 3700 50  0000 C CNN
	1    9950 3700
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x18_Odd_Even P1
U 1 1 56D743B5
P 9700 5050
F 0 "P1" H 9700 6000 50  0000 C CNN
F 1 "Digital" V 9700 5050 50  0000 C CNN
F 2 "Socket_Arduino_Mega:Socket_Strip_Arduino_2x18" H 9700 4000 50  0001 C CNN
F 3 "" H 9700 4000 50  0000 C CNN
	1    9700 5050
	0    -1   1    0   
$EndComp
Wire Wire Line
	9100 1200 9100 1650
Wire Wire Line
	9250 1450 9250 1200
Wire Wire Line
	9350 1450 9250 1450
Wire Notes Line
	10450 1000 10450 500 
Wire Notes Line
	11200 1000 10450 1000
Wire Notes Line
	9850 650  9850 475 
Wire Notes Line
	8350 650  9850 650 
Wire Wire Line
	9100 1650 9350 1650
Wire Wire Line
	9000 1050 9000 1750
Wire Wire Line
	9000 1750 9350 1750
Wire Wire Line
	9350 2050 8900 2050
Wire Wire Line
	8900 2050 8900 1200
Wire Wire Line
	8600 1550 9350 1550
Wire Wire Line
	9350 1850 9250 1850
Wire Wire Line
	9250 1850 9250 1950
Wire Wire Line
	9250 1950 9250 2150
Wire Wire Line
	9350 1950 9250 1950
Connection ~ 9250 1950
Wire Wire Line
	10150 1150 10400 1150
Wire Wire Line
	10400 1250 10150 1250
Wire Wire Line
	10150 1350 10400 1350
Wire Wire Line
	10150 1550 10400 1550
Wire Wire Line
	10400 1650 10150 1650
Wire Wire Line
	10150 1750 10400 1750
Wire Wire Line
	10150 1850 10400 1850
Wire Wire Line
	10400 1950 10150 1950
Wire Wire Line
	10150 2050 10400 2050
Wire Wire Line
	10250 2150 10250 1450
Wire Wire Line
	10250 1450 10150 1450
Wire Wire Line
	9350 2450 8900 2450
Wire Wire Line
	8900 2550 9350 2550
Wire Wire Line
	9350 2650 8900 2650
Wire Wire Line
	8900 2750 9350 2750
Wire Wire Line
	9350 2850 8900 2850
Wire Wire Line
	8900 2950 9350 2950
Wire Wire Line
	9350 3050 8900 3050
Wire Wire Line
	8900 3150 9350 3150
Wire Wire Line
	10400 2450 10150 2450
Wire Wire Line
	10150 2550 10400 2550
Wire Wire Line
	10400 2650 10150 2650
Wire Wire Line
	10150 2750 10400 2750
Wire Wire Line
	10400 2850 10150 2850
Wire Wire Line
	10150 2950 10400 2950
Wire Wire Line
	10400 3050 10150 3050
Wire Wire Line
	10150 3150 10400 3150
Wire Wire Line
	9350 3400 8900 3400
Wire Wire Line
	8900 3500 9350 3500
Wire Wire Line
	9350 3600 8900 3600
Wire Wire Line
	8900 3700 9350 3700
Wire Wire Line
	9350 3800 8900 3800
Wire Wire Line
	8900 3900 9350 3900
Wire Wire Line
	9350 4000 8900 4000
Wire Wire Line
	8900 4100 9350 4100
Wire Wire Line
	10400 3400 10150 3400
Wire Wire Line
	10150 3500 10400 3500
Wire Wire Line
	10400 3600 10150 3600
Wire Wire Line
	10150 3700 10400 3700
Wire Wire Line
	10400 3800 10150 3800
Wire Wire Line
	10150 3900 10400 3900
Wire Wire Line
	10400 4000 10150 4000
Wire Wire Line
	10150 4100 10400 4100
Wire Wire Line
	10500 4850 10500 4650
Wire Wire Line
	10400 4850 10400 4650
Wire Wire Line
	10300 4850 10300 4650
Wire Wire Line
	10200 4850 10200 4650
Wire Wire Line
	10100 4850 10100 4650
Wire Wire Line
	10000 4850 10000 4650
Wire Wire Line
	9900 4850 9900 4650
Wire Wire Line
	9800 4850 9800 4650
Wire Wire Line
	9700 4850 9700 4650
Wire Wire Line
	9600 4850 9600 4650
Wire Wire Line
	9500 4850 9500 4650
Wire Wire Line
	9400 4850 9400 4650
Wire Wire Line
	9300 4850 9300 4650
Wire Wire Line
	9200 4850 9200 4650
Wire Wire Line
	9100 4850 9100 4650
Wire Wire Line
	9000 4850 9000 4650
Wire Wire Line
	10500 5350 10500 5650
Wire Wire Line
	10400 5350 10400 5650
Wire Wire Line
	10300 5350 10300 5650
Wire Wire Line
	10200 5350 10200 5650
Wire Wire Line
	10100 5350 10100 5650
Wire Wire Line
	10000 5350 10000 5650
Wire Wire Line
	9900 5350 9900 5650
Wire Wire Line
	9800 5350 9800 5650
Wire Wire Line
	9700 5350 9700 5650
Wire Wire Line
	9600 5350 9600 5650
Wire Wire Line
	9500 5350 9500 5650
Wire Wire Line
	9400 5350 9400 5650
Wire Wire Line
	9300 5350 9300 5650
Wire Wire Line
	9200 5350 9200 5650
Wire Wire Line
	9100 5350 9100 5750
Wire Wire Line
	9000 5350 9000 5750
Wire Wire Line
	8900 4850 8650 4850
$Comp
L power:GND #PWR05
U 1 1 56D758F6
P 8650 5750
F 0 "#PWR05" H 8650 5500 50  0001 C CNN
F 1 "GND" H 8650 5600 50  0000 C CNN
F 2 "" H 8650 5750 50  0000 C CNN
F 3 "" H 8650 5750 50  0000 C CNN
	1    8650 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 5350 8650 5350
Connection ~ 8650 5350
Wire Wire Line
	10750 5350 10600 5350
Wire Wire Line
	10750 4850 10600 4850
$Comp
L power:+5V #PWR06
U 1 1 56D75AB8
P 10750 4550
F 0 "#PWR06" H 10750 4400 50  0001 C CNN
F 1 "+5V" H 10750 4690 50  0000 C CNN
F 2 "" H 10750 4550 50  0000 C CNN
F 3 "" H 10750 4550 50  0000 C CNN
	1    10750 4550
	1    0    0    -1  
$EndComp
Connection ~ 10750 4850
Wire Wire Line
	10750 4550 10750 4850
Wire Wire Line
	10750 4850 10750 5350
Wire Wire Line
	8650 4850 8650 5350
Wire Wire Line
	8650 5350 8650 5750
Wire Notes Line
	11200 6050 8350 6050
Wire Notes Line
	8350 6050 8350 500 
$Comp
L MX29F1615:MX29F1615 U1
U 1 1 5F46E6BE
P 4475 2300
F 0 "U1" H 4475 2815 50  0000 C CNN
F 1 "MX29F1615" H 4475 2724 50  0000 C CNN
F 2 "Package_DIP:DIP-42_W15.24mm_Socket" H 4475 2300 50  0001 C CNN
F 3 "" H 4475 2300 50  0001 C CNN
	1    4475 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5F46E9F5
P 3350 3150
F 0 "#PWR0101" H 3350 2900 50  0001 C CNN
F 1 "GND" H 3355 2977 50  0000 C CNN
F 2 "" H 3350 3150 50  0001 C CNN
F 3 "" H 3350 3150 50  0001 C CNN
	1    3350 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3975 2050 3525 2050
Text Label 3750 2050 0    50   ~ 0
12(**)
Wire Wire Line
	3975 2150 3525 2150
Text Label 3750 2150 0    50   ~ 0
11(**)
Wire Wire Line
	3975 2250 3525 2250
Text Label 3750 2250 0    50   ~ 0
A7
Wire Wire Line
	3975 2350 3525 2350
Text Label 3750 2350 0    50   ~ 0
A6
Wire Wire Line
	3975 2450 3525 2450
Text Label 3750 2450 0    50   ~ 0
A5
Wire Wire Line
	3975 2550 3525 2550
Text Label 3750 2550 0    50   ~ 0
A4
Wire Wire Line
	3975 2650 3525 2650
Text Label 3750 2650 0    50   ~ 0
A3
Wire Wire Line
	3975 2750 3525 2750
Text Label 3750 2750 0    50   ~ 0
A2
Wire Wire Line
	3975 3250 3525 3250
Text Label 3750 3250 0    50   ~ 0
2(**)
Wire Wire Line
	3975 3350 3525 3350
Text Label 3750 3350 0    50   ~ 0
22
Wire Wire Line
	3975 3450 3525 3450
Text Label 3750 3450 0    50   ~ 0
37
Wire Wire Line
	3975 3550 3525 3550
Text Label 3750 3550 0    50   ~ 0
23
Wire Wire Line
	3975 3650 3525 3650
Text Label 3750 3650 0    50   ~ 0
36
Wire Wire Line
	3975 3750 3525 3750
Text Label 3750 3750 0    50   ~ 0
24
Wire Wire Line
	3975 3850 3525 3850
Text Label 3750 3850 0    50   ~ 0
35
Wire Wire Line
	3975 3950 3525 3950
Text Label 3750 3950 0    50   ~ 0
25
Wire Wire Line
	3975 2850 3525 2850
Text Label 3750 2850 0    50   ~ 0
A1
Wire Wire Line
	3975 2950 3525 2950
Text Label 3750 2950 0    50   ~ 0
A0
Wire Wire Line
	3975 3050 3525 3050
Text Label 3750 3050 0    50   ~ 0
3(**)
Wire Wire Line
	5425 2050 4975 2050
Text Label 5200 2050 0    50   ~ 0
13(**)
Wire Wire Line
	5425 2150 4975 2150
Text Label 5200 2150 0    50   ~ 0
A8
Wire Wire Line
	5425 2250 4975 2250
Text Label 5200 2250 0    50   ~ 0
A9
Wire Wire Line
	5425 2350 4975 2350
Text Label 5200 2350 0    50   ~ 0
A10
Wire Wire Line
	5425 2450 4975 2450
Text Label 5200 2450 0    50   ~ 0
A11
Wire Wire Line
	5425 2550 4975 2550
Text Label 5200 2550 0    50   ~ 0
A12
Wire Wire Line
	5425 2650 4975 2650
Text Label 5200 2650 0    50   ~ 0
A13
Wire Wire Line
	5425 2750 4975 2750
Text Label 5200 2750 0    50   ~ 0
A14
Wire Wire Line
	5425 2850 4975 2850
Text Label 5200 2850 0    50   ~ 0
A15
Wire Wire Line
	5425 2950 4975 2950
Text Label 5200 2950 0    50   ~ 0
10(**)
Wire Wire Line
	5425 3050 4975 3050
Text Label 775  3325 0    50   ~ 0
4(**)
Wire Wire Line
	3975 4050 3525 4050
Text Label 3750 4050 0    50   ~ 0
34
Wire Wire Line
	5625 3150 4975 3150
Wire Wire Line
	5425 3250 4975 3250
Text Label 5200 3250 0    50   ~ 0
30
Wire Wire Line
	5425 3350 4975 3350
Text Label 5200 3350 0    50   ~ 0
29
Wire Wire Line
	5425 3450 4975 3450
Text Label 5200 3450 0    50   ~ 0
31
Wire Wire Line
	5425 3550 4975 3550
Text Label 5200 3550 0    50   ~ 0
28
Wire Wire Line
	5425 3650 4975 3650
Text Label 5200 3650 0    50   ~ 0
32
Wire Wire Line
	5425 3750 4975 3750
Text Label 5200 3750 0    50   ~ 0
27
Wire Wire Line
	5425 3850 4975 3850
Text Label 5200 3850 0    50   ~ 0
33
Wire Wire Line
	5425 3950 4975 3950
Text Label 5200 3950 0    50   ~ 0
26
Wire Wire Line
	3350 3150 3975 3150
$Comp
L power:GND #PWR0102
U 1 1 5F4AE261
P 5625 3150
F 0 "#PWR0102" H 5625 2900 50  0001 C CNN
F 1 "GND" H 5630 2977 50  0000 C CNN
F 2 "" H 5625 3150 50  0001 C CNN
F 3 "" H 5625 3150 50  0001 C CNN
	1    5625 3150
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LM7810_TO220 U2
U 1 1 5F4BBD5A
P 1550 1025
F 0 "U2" H 1550 1267 50  0000 C CNN
F 1 "LM7810_TO220" H 1550 1176 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 1550 1250 50  0001 C CIN
F 3 "http://www.fairchildsemi.com/ds/LM/LM7805.pdf" H 1550 975 50  0001 C CNN
	1    1550 1025
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 5F4BBEFD
P 925 1175
F 0 "C1" H 1043 1221 50  0000 L CNN
F 1 "CP" H 1043 1130 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P2.00mm" H 963 1025 50  0001 C CNN
F 3 "~" H 925 1175 50  0001 C CNN
	1    925  1175
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5F4BBFD3
P 2175 1175
F 0 "C2" H 2293 1221 50  0000 L CNN
F 1 "CP" H 2293 1130 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P2.00mm" H 2213 1025 50  0001 C CNN
F 3 "~" H 2175 1175 50  0001 C CNN
	1    2175 1175
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5F4BC074
P 1550 1525
F 0 "#PWR0104" H 1550 1275 50  0001 C CNN
F 1 "GND" H 1555 1352 50  0000 C CNN
F 2 "" H 1550 1525 50  0001 C CNN
F 3 "" H 1550 1525 50  0001 C CNN
	1    1550 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 1525 1550 1425
Wire Wire Line
	925  1325 925  1425
Wire Wire Line
	2175 1425 2175 1325
Wire Wire Line
	925  1425 1550 1425
Connection ~ 1550 1425
Wire Wire Line
	1550 1425 1550 1325
Wire Wire Line
	1550 1425 2175 1425
Wire Wire Line
	1850 1025 2175 1025
$Comp
L power:+10V #PWR0105
U 1 1 5F4EC290
P 2325 1025
F 0 "#PWR0105" H 2325 875 50  0001 C CNN
F 1 "+10V" H 2340 1198 50  0000 C CNN
F 2 "" H 2325 1025 50  0001 C CNN
F 3 "" H 2325 1025 50  0001 C CNN
	1    2325 1025
	1    0    0    -1  
$EndComp
Wire Wire Line
	2325 1025 2175 1025
Connection ~ 2175 1025
Wire Wire Line
	925  1025 1250 1025
Connection ~ 925  1025
Wire Wire Line
	700  1025 925  1025
$Comp
L Device:Q_NPN_EBC Q1
U 1 1 5F519D37
P 1575 3325
F 0 "Q1" H 1766 3371 50  0000 L CNN
F 1 "Q_NPN_EBC" H 1766 3280 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-18-3" H 1775 3425 50  0001 C CNN
F 3 "~" H 1575 3325 50  0001 C CNN
	1    1575 3325
	1    0    0    -1  
$EndComp
$Comp
L Device:R 10K3
U 1 1 5F519E70
P 1675 3725
F 0 "10K3" H 1745 3771 50  0000 L CNN
F 1 "R" H 1745 3680 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 1605 3725 50  0001 C CNN
F 3 "~" H 1675 3725 50  0001 C CNN
	1    1675 3725
	1    0    0    -1  
$EndComp
Wire Wire Line
	1675 3575 1675 3525
$Comp
L power:GND #PWR0107
U 1 1 5F521794
P 1675 3925
F 0 "#PWR0107" H 1675 3675 50  0001 C CNN
F 1 "GND" H 1680 3752 50  0000 C CNN
F 2 "" H 1675 3925 50  0001 C CNN
F 3 "" H 1675 3925 50  0001 C CNN
	1    1675 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	1675 3925 1675 3875
$Comp
L Device:R 10K1
U 1 1 5F5290CB
P 1150 3325
F 0 "10K1" V 943 3325 50  0000 C CNN
F 1 "R" V 1034 3325 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 1080 3325 50  0001 C CNN
F 3 "~" H 1150 3325 50  0001 C CNN
	1    1150 3325
	0    1    1    0   
$EndComp
$Comp
L Device:R 10K2
U 1 1 5F52915A
P 1675 2925
F 0 "10K2" H 1605 2879 50  0000 R CNN
F 1 "R" H 1605 2970 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 1605 2925 50  0001 C CNN
F 3 "~" H 1675 2925 50  0001 C CNN
	1    1675 2925
	-1   0    0    1   
$EndComp
Wire Wire Line
	1300 3325 1375 3325
Wire Wire Line
	1675 3125 1675 3100
$Comp
L power:+10V #PWR0108
U 1 1 5F5389F1
P 1675 2725
F 0 "#PWR0108" H 1675 2575 50  0001 C CNN
F 1 "+10V" H 1690 2898 50  0000 C CNN
F 2 "" H 1675 2725 50  0001 C CNN
F 3 "" H 1675 2725 50  0001 C CNN
	1    1675 2725
	1    0    0    -1  
$EndComp
Wire Wire Line
	1675 2725 1675 2775
Text Label 5200 3050 0    50   ~ 0
BYTEVPP
Text Label 1900 3100 0    50   ~ 0
BYTEVPP
Wire Wire Line
	1000 3325 650  3325
Connection ~ 1675 3100
Wire Wire Line
	1675 3100 1675 3075
Wire Wire Line
	1675 3100 2200 3100
Text Label 700  1025 0    50   ~ 0
Vin
$Comp
L power:+5V #PWR?
U 1 1 5F563431
P 5625 4050
F 0 "#PWR?" H 5625 3900 50  0001 C CNN
F 1 "+5V" H 5640 4223 50  0000 C CNN
F 2 "" H 5625 4050 50  0001 C CNN
F 3 "" H 5625 4050 50  0001 C CNN
	1    5625 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4975 4050 5625 4050
$EndSCHEMATC
