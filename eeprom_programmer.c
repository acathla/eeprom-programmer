#include <stdio.h>		// Standard input/output definitions
#include <stdlib.h>
#include <string.h>		// String function definitions
#include <unistd.h>		// for usleep()
#include <limits.h>		// CHAR_BIT, UCHAR_MAX
#include "eeprom_programmer-lib.h"

#define ADDR_MAX 0x1FFFFF	// 2^(number of address lines + 1 (bot 8 bit read))-1, 2^21-1
#define ACK 0x06
#define NAK 0x15

void
usage (void)
{
  printf
    ("Usage: mx29f1615_prog [-r <file> <addr> <size> | -w <file> <addr> <size> <offset> | -e ]\n"
     "\n"
     " -r to read the rom from address to address+size and write the result in <file>\n"
     " -R to read the device ID\n"
     " -w to write <file> to eeprom at write offset, starting to read file at <address>, <size> is length to write or size max of file if only one argument.\n"
     " -e to erase the entire chip (beware, only 100 erase/program cycles\n");
  exit (EXIT_SUCCESS);
}

void
error (char *msg)
{
  fprintf (stderr, "%s\n", msg);
  exit (EXIT_FAILURE);
}

void
inttochar (int num, uint8_t * dest)
{
  int i;
  for (i = 0; i < 3; i++)
    {
      dest[i] = num & UCHAR_MAX;
      num >>= CHAR_BIT;
    }
}

void
write_bytes_from_int (int fd, uint32_t a)
{
  uint8_t a8[4];	// last byte is useless here
  inttochar (a, a8);
  serialport_writebyte (fd, a8[0]);
  serialport_writebyte (fd, a8[1]);
  serialport_writebyte (fd, a8[2]);
}

void
write_bytes_from_int16 (int fd, uint16_t a)
{
  uint8_t a8[2];
  inttochar (a, a8);
  serialport_writebyte (fd, a8[0]);
  serialport_writebyte (fd, a8[1]);
}

int
write_data (int fd, uint32_t addr, uint16_t size, uint8_t * data)
{
  uint8_t buf[256];
  if (serialport_writebyte (fd, '\x0D') != 0)
    {				// Write data to opbuf
      printf ("Error sending 0x0D\n");
    }
  write_bytes_from_int (fd, addr);	// Write 24 bits start address
  write_bytes_from_int16 (fd, size);	// Write 16 bits size
  for (uint16_t i = 0; i < size; i++)
    {
      if (serialport_writebyte (fd, data[i]) != 0)
	{
	  //printf("Error sending data %d\n",i);
	  usleep (100000);
	  i--;
	}
    }
  serialport_read_bytes (fd, buf, 1, 100000);
  if (buf[0] != ACK)
    {
      printf ("write_data: Error, no ACK read, %hhx read instead\n", buf[0]);
      return -1;
    }
  return 0;
}

int
write_command (int fd, uint32_t addr, uint8_t cmd)
{
  uint8_t buf[1];
  serialport_writebyte (fd, '\x0C');	// Write to opbuf a single byte
  write_bytes_from_int (fd, addr);	// Write 24 bits address
  serialport_writebyte (fd, cmd);
  serialport_read_bytes (fd, buf, 1, 10000);
  if (buf[0] != ACK)
    {
      printf ("Write_command: Error, no ACK read, %hhx read instead\n",
	      buf[0]);
      return -1;
    }
  return 0;
}

int
flash_read_bytes (int fd, uint32_t addr, uint32_t size, uint8_t * buffer)
{
  uint8_t buf[1];
  serialport_writebyte (fd, '\x42');	// Read n bytes
  write_bytes_from_int (fd, addr);	// Write 24 bits address
  write_bytes_from_int (fd, size);	// Length of bytes to read (24 bits)
  serialport_read_bytes (fd, buf, 1, 100);
  if (buf[0] != ACK)
    {
      printf ("Read_bytes : Error, no ACK read, %hhx read instead\n", buf[0]);
      return -1;
    }
  return serialport_read_bytes (fd, buffer, size, 10000);
}

int
send_command_sequence (int fd)
{
  if (write_command (fd, 0x5555, '\xAA') != 0)
    return -1;
  if (write_command (fd, 0x2AAA, '\x55') != 0)
    return -1;
  return 0;
}

int
clearSR (int fd)
{
  // Clear Status Register Mode
  send_command_sequence (fd);
  serialport_writebyte (fd, '\x0C');	// Write to opbuf byte
  if (write_command (fd, 0x5555, '\x50') != 0)
  {
    printf ("CleaSR Error sending command 0x50\n");
    return -1;
  }
  return 0;
}

int
read_mode (int fd)
{
  send_command_sequence (fd);
  if (write_command (fd, 0x5555, '\xF0') != 0)
    {
      printf ("read_mode error sending command 0xF0\n");
      return (-1);
    }
  return 0;
}

int
main (int argc, char *argv[])
{
  const int buf_max = 256;
  const uint16_t mtu = 128;
  uint8_t bigbuf[ADDR_MAX + 1];
  // uint8_t bigbuf2[ADDR_MAX+1];
  static int fd = -1;
  static FILE *file = 0;
  char *serialport = "/dev/ttyUSB0";
  char cmd;
  //int baudrate = 115200;  // default
  int baudrate = 1000000;	// default
  uint8_t buf[buf_max];
  uint32_t addr_s, size_w, offset;
  int err;
  uint16_t incr;

  if (argc == 1)
    {
      usage ();
    }
  if (argc >= 2)
    {
      // Open the serial port and read the name of the programmer
      fd = serialport_init (serialport, baudrate);
      if (fd == -1)
	{
	  error ("couldn't open port");
	  exit (-1);
	}
      serialport_flush (fd);
      serialport_writebyte (fd, '\3');
      serialport_read_bytes (fd, buf, 1, 100);
      if (buf[0] != ACK)
	{
	  printf ("Error, no ACK read\n");
	  return (-1);
	}
      serialport_read_bytes (fd, buf, 16, 100);
      buf[15] = '\0';
      printf ("Programmer : %s\n", buf);
      if (strlen (argv[1]) == 2)
	cmd = argv[1][1];	// to ignore the '-' of '-w', -r', etc
      else
	cmd = argv[1][0];	// or not
      switch (cmd)
	{
	case 'e':
	  if (read_mode (fd) != 0)
	    exit (-1);

	  printf ("Sending commands to erase chip...\n");
	  if (argc == 3)
	    {			// sector erase
	      addr_s = strtol (argv[2], NULL, 10);
	    }
	  if (send_command_sequence (fd) != 0)
	    printf ("Error sending command sequence\n");
	  if (write_command (fd, 0x5555, '\x80') != 0)
	    printf ("Error sending command 0xA0\n");
	  if (write_command (fd, 0x5555, '\xAA') != 0)
	    printf ("Error sending command 0xA0\n");
	  if (write_command (fd, 0x2AAA, '\x55') != 0)
	    printf ("Error sending command 0xA0\n");

	  if (argc == 3)
	    {			// sector erase
	      if (write_command (fd, addr_s, '\x30') != 0)
		printf ("Error sending command 0x30\n");
	    }
	  else
	    {
	      if (write_command (fd, 0x5555, '\x10') != 0)
		printf ("Error sending command 0x10\n");
	    }
	  buf[0] = 0;
	  while (buf[0] == 0)
	    {
	      flash_read_bytes (fd, 0, 1, buf);
	      sleep (1);
	    }
	  if (buf[0] == 0x90)
	    {
	      printf ("Error erasing the device !! :(\n");
	      clearSR (fd);
	    }
	  if (buf[0] == 0x80)
	    {
	      read_mode (fd);
	    }
	  break;
	case 'r':
	  addr_s = size_w = 0;
	  if (argc < 3)
	    {
	      printf ("Wrong number of arguments\n");
	      return (-1);
	    }
	  if (argc == 5)
	    {
	      if (sscanf (argv[3], "%x", &addr_s) == 0)
		{
		  addr_s = strtol (argv[3], NULL, 10);
		}
	      if (sscanf (argv[4], "%x", &size_w) == 0)
		{
		  size_w = strtol (argv[4], NULL, 10);
		}
	    }
	  if (argc == 4)
	    {			// No size, read from start address to the end of the chip
	      if (sscanf (argv[3], "%x", &addr_s) == 0)
		{
		  addr_s = strtol (argv[3], NULL, 10);
		}
	      size_w = ADDR_MAX - addr_s;
	    }
	  if (argc == 3)
	    {			// No size, no start address => read the full chip
	      printf ("Reading full chip\n");
	      addr_s = 0;
	      size_w = ADDR_MAX + 1;
	    }

	  if (addr_s + size_w > ADDR_MAX + 1)
	    {
	      printf ("Error : trying to read too far\n");
	      return -1;
	    }
	  file = fopen (argv[2], "w");
	  if (file == 0)
	    {
	      printf ("Error opening the file %s\n", argv[2]);
	      return -1;
	    }
	  printf
	    ("Reading from chip at address 0x%08x, 0x%08x bytes to file %s\n",
	     addr_s, size_w, argv[2]);

	  // Read size_w bytes, by 128 bytes if size_w > mtu
	  for (uint32_t i = addr_s; i < addr_s + size_w;)
	    {
	      if (i + 128 < addr_s + size_w)
		incr = 128;
	      else
		incr = addr_s + size_w - i;
	      putchar ('.');
	      fflush (stdout);
	      //printf("flash_read_bytes(fd, 0x%08x, 0x%04x, bigbuf+i)\n",i, incr);
	      if ((err = flash_read_bytes (fd, i, incr, bigbuf + i)) != 0)
		{
		  printf ("Error : read_bytes failed, error : %d\n", err);
		  return -1;
		}
	      i += incr;
	    }
	  if (fwrite (bigbuf + addr_s, sizeof (uint8_t), size_w, file))
	    {
	      printf ("Successfully written %d bytes to the file %s\n", size_w,
		      argv[2]);
	    }
	  break;

	case 'w':
	  //     w <file> <addr> <size>
	  //argv 1   2      3      4
	  addr_s = size_w = offset = 0;
	  if (argc < 5)
	    {
	      printf ("Wrong number of arguments\n");
	      return (-1);
	    }
	  file = fopen (argv[2], "r");
	  if (argc >= 5)
	    {
	      if (sscanf (argv[3], "%x", &addr_s) == 0)
		{
		  addr_s = strtol (argv[3], NULL, 10);
		}
	      if (sscanf (argv[4], "%x", &size_w) == 0)
		{
		  size_w = strtol (argv[4], NULL, 10);
		}
	    }
	  if (argc == 6)
	    {			// offset in read file
	      if (sscanf (argv[5], "%x", &offset) == 0)
		{
		  offset = strtol (argv[5], NULL, 10);
		}
	    }
	  if (addr_s + size_w + offset > ADDR_MAX + 1)
	    {
	      printf ("Error : trying to write too far\n");
	      exit (-1);
	    }
	  // Reading the file to bigbuf
	  if (!fread (bigbuf, sizeof (uint8_t), size_w + addr_s, file))
	    {
	      printf ("Error reading the file %s\n", argv[2]);
	      exit (-1);
	    }
	  printf ("Write to chip at address 0x%08x, 0x%08x bytes\n",
		  addr_s + offset, size_w);
	  for (uint32_t i = addr_s; i < addr_s + size_w;)
	    {
	      if (i + mtu < addr_s + size_w)
		incr = mtu;
	      else
		incr = addr_s + size_w - i;
	      putchar ('.');
	      fflush (stdout);
	      if ((write_data (fd, i + offset, incr, bigbuf + i)) != 0)
		{
		  printf ("Error : write_data failed\n");
		  return -1;
		}
	      i += incr;
	    }
	  break;
	case 'R':		// read device ID
	  if (send_command_sequence (fd) != 0)
	    {
	      printf ("Error sending command sequence\n");
	      exit (-1);
	    }
	  if (write_command (fd, 0x5555, '\x90') != 0)
	    {
	      printf ("Error sendin command 0x90 (Read ID)\n");
	      exit (-1);
	    }
	  printf ("Sent read ID\n");
	  flash_read_bytes (fd, 0, 1, buf);
	  printf ("ID : %hhx ", buf[0]);
	  flash_read_bytes (fd, 1, 1, buf);
	  printf ("%hhx \n", buf[0]);
	  if (buf[0] == 0x90 && buf[1] == 0x90)
	    clearSR (fd);
	  read_mode (fd);
	  break;
	case 'C':		// Clear Status Register
	  clearSR (fd);
	  break;
	default:
	  usage ();
	}
    }
  if (file != 0)
    fclose (file);
}
